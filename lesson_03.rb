#!/usr/bin/env ruby

# CLASS INHERITANCE

class Pet
  attr_accessor :name, :voice, :color
  def initialize(name)
    @name = name
    @voice = "undefined"
    @color = "undefined"
  end
  
  def sound
    puts voice
  end
end

class Dog < Pet
  def initialize(name)
    super(name)
    @voice = "Yap!"
    @color = "black"
  end
  
end

class Cat < Pet
  attr_accessor :bell
  def initialize(name)
    super(name)
    @voice = "Yap!"
    @color = "black"
    @bell = false
  end
  
  def scratch
    puts "#{@name} is scratching"
  end
end


fido = Dog.new("Fido")
p fido
puts
leo = Cat.new("Leo")
p leo





