#!/usr/bin/env ruby

# CamelCase for constants or camelCase for variables
# snake_case for vars or SNAKE_CASE for CONSTANTS


# Arrays: ordered lists of heterogeneous objects
an_array = [6, 12, 8, 3]
mix_array = ["string 1", 2, an_array]

puts an_array
p an_array
p mix_array

# i-th element of an_array: an_array[i]
puts "first element of an_array is #{an_array[0]}"
puts "last element of an_array is #{an_array[-1]}"

# replace/change an entry/item
an_array[0] = 0
an_array[10] = "exaggerated"
p an_array

# length of an array: an_array.size
puts "an_array has #{an_array.size} elements"
puts "mix_array has #{mix_array.size} elements"

# sub-arrays: use ranges like 3..5
p an_array[3..5]

# Hashes: unordered lists of items (set of key-value pairs)
a_hash = {"a" => 1, "b" => 2, "c" => 3, "a" => 7}
p a_hash["b"]
a_hash["d"] = 4
a_hash["e"] = 5
p a_hash

# Symbols: simplified strings (valis symbol: :a_symbol)
a_hash = {:a => 1, :b => 2, :c => 3}
a_hash = {a:1, b:2, c:3, another_key:10}
p a_hash

user1 = {name:"Paolo", surname:"Bosetti"}
user2 = {name:"John", surname:"Carter"}
users = [user1, user2]

p users
puts users[0][:name]

tools = []
tools << {length:100.0, diam:10.0, n:5, name:"rougning mill"}
tools << {length:120.0, diam:8.0, n:5, name:"finishing mill"}

puts
puts "Tools table: #{tools}"
puts
puts "Tool 1: #{tools[1]}"
puts "Length of tool 1: #{tools[1][:length]}"





