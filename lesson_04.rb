#!/usr/bin/env ruby

# Methods of Fixnum, and ITERATORS

# Conditionals

a = 10

if a > 10  
  puts "a is ten"
elsif a == 11 
  puts "a is eleven"
elsif a == 12 then
  puts "a is twelve"
else
  puts "a has an unexpected value"
end

b = 2 * 5

# Loops

10.times do |i|
  puts "#{i}: Hello!"
end

puts

1.upto(10) do |i|
  puts "#{i}: Hello!"
end

10.downto(1) do |i|
  puts "#{i}: Hello!"
end

# Iterators
ary = [1,"2",3,"Hello!",5,6]

0.upto(ary.length-1) do |i|
  puts ary[i]
end

ary.each do |e|
  puts e
end

puts 

ary.each_with_index do |e,i|
  puts "ary[#{i}] => #{e}"
end

puts 
hsh = {a:1, b:2, c:"Hello"}
hsh.each do |key,value|
  puts "#{key} => #{value}"
end










