#!/usr/bin/env ruby

# Load the base library under lib:
require "./lib/rnc.rb"
require "./lib/machine2.rb"
require "./MTViewer/viewer.rb"


# ARGV is the array of command line arguments
unless ARGV.size == 1 then
  warn "I need the name of a G-code file as argument!"
  exit
end

# Define the configuration Hash:
CFG = {
  :file => ARGV[0],
  :A    => 200,
  :D    => 500,
  :tq   => 0.005,
  :tq_corr => (0.077 / 100),
  :max_pos_error => 0.005
}

# Instantiate the machine tool dynamics simulator
m = RNC::Machine.new
m.load_configs(["lib/X.yaml", "lib/Y.yaml", "lib/Z.yaml"])
m.go_to RNC::Point[0,0,0]
m.reset

# Instantiate the machine viewer GUI
viewer = Viewer::Link.new("./MTViewer/linux/MTviewer")
viewer.go_to RNC::Point[0,0,0]


# Instantiate a new Parser
parser = RNC::Parser.new(CFG)
parser.parse_file

# Format string for file output:
format = "%7.3f %7.3f %3d %6.4f %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f"

puts "=" * 79
puts "Press SPACE in the viewer window to start"
puts "=" * 79
loop until (viewer.run)


File.open("profiles.txt", "w") do |data_file|
  
  # Initial execution time
  start_time = Time.now
  
  # Loop over all G-Code blocks in parser
  # the two block parameters are:
  # iterp:   the interpolator: it has a link to the current block and can loop 
  #          over time steps
  # n_block: the current block number
  parser.each_block do |interp, n_block|
    puts "N#{n_block}: #{interp.block.inspect}"
    # Check current block type:
    case interp.block.type
    when :G00 # Rapid movement
      # Set axes set-points to final target position, then wait for
      # axes to reach it
      m.go_to(interp.block.target.map {|e| e / 1000.0})
      error = m.error * 1000 # convert from m  to mm
      
      block_start_time = Time.now - start_time
      while (error >= CFG[:max_pos_error]) do 
        sleep_thread = Thread.new { sleep (CFG[:tq] - CFG[:tq_corr]) }
        
        now = Time.now - start_time
        state = m.step! # forward integration of dynamics equations
        state[:pos].map! {|e| e * 1000} # Convert from m to mm
        error = m.error * 1000
        
        # Build the data array:
        data = [
          now,
          now - block_start_time,
          n_block,
          1 - (error / interp.block.length),
          interp.block.target,
          state[:pos]
        ].flatten
        
        # Update viewr position
        viewer.go_to state[:pos]
        
        # Print the formatted output into data_file:
        output_string = format % data
        data_file.puts output_string 
        #puts output_string
        
        # Wait for the timing thread to finish:
        sleep_thread.join
      end
      
      
    when :G01 # Linear interpolation
      interp.each_timestep do |t, cmd|
        # cmd is a hash with fileds :position, :lambda, :type
        # starts the timing thread
        sleep_thread = Thread.new { sleep (CFG[:tq] - CFG[:tq_corr]) }
        
        # now is the number of seconds elapsed since the part program start
        now = Time.now - start_time
        
        # Send the set-points for machine axes to cmd[:X], cmd[:Y] and cmd[:Z]        
        m.go_to(cmd[:position].map {|e| e / 1000.0}) # Convert mm to m
        
        # Ask the dynamics simulator to make one forward integration step
        # The integrator is set to make integration steps of 5 ms, this must
        # correspond to CFG[:tq]
        state = m.step!
        state[:pos].map! {|e| e * 1000} # Convert in-place from m to mm
        
        # Collect all info into one single array:
        data = [
          now,                # total time
          t,                  # block time
          n_block,            # block number
          cmd[:lambda],       # current value of lambda function
          cmd[:position],     # Nominal position
          state[:pos]         # Actual position
        ].flatten
        
        # Update viewer position
        viewer.go_to state[:pos]
        
        # Print the formatted output into data_file:
        output_string = format % data
        data_file.puts output_string 
        #puts output_string
        
        # Wait for the timing thread to finish:
        sleep_thread.join
      end
    else # Unsupported
      warn "Skipping unsupported block #{interp.block.line}"
    end
    
  end # blocks iteration
end # close the data file


puts "=" * 79
puts "Press SPACE in the viewer window to stop"
puts "=" * 79
loop while (viewer.run)

viewer.close



