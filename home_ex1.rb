#!/usr/bin/env ruby

class Tool
  attr_reader :number, :diameter, :length, :teeth, :n, :fz, :type
  attr_reader :v
  
  def initialize(number, type)
    @number = number
    self.diameter = 10
    @length = 10
    @teeth = 3
    @type = type
    @fz = 0.1
  end
  
  def diameter=(v)
    @diameter = v
    @v = @diameter*@n*Math::PI)
  end
  
  def fr
    return (fz*@n*@teeth).round(2)
  end
  
  def inspect
    return "Id.=#{@number}, D=#{@diameter}, L=#{@length}, nTeeth=#{@teeth}, n=#{@n}, fz=#{@fz}, type=#{@type}, v=#{self.v}, fr=#{self.fr}"
  end
    
end

t1 = Tool.new(1,'finishing')
t1.diameter = 12
t1.length = 45
t1.teeth = 5
t1.n = 1000

puts
p t1  # p(a) corresponds to puts(a.inspect)

puts
puts t1.v # => current cutting speed




