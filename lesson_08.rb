#!/usr/bin/env ruby

# Timings


format = "i: %d time: %9.3f"
start_time = Time.now
100.times do |i|
  sleep_thread = Thread.new { sleep (0.005 - (0.077 / 100)) }
  
  now = Time.now - start_time
  puts format % [i, now]
  
  sleep_thread.join
end
