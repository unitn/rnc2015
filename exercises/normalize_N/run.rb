#!/usr/bin/env ruby
require "./lib/normalize_gcode.rb"

raise RuntimeError, "Usage: normalize.rb INPUT OUTPUT" if ARGV.size != 2

gcm = NormalizeGCode.new(ARGV[0])
puts "converted file:"
puts gcm.output
gcm.write(ARGV[1])