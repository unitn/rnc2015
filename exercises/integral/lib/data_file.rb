#!/usr/bin/env ruby


class DataFile
  attr_reader :data
  attr_accessor :x_col, :y_col
  def initialize(data_file)
    @data = [] # data array: a list of rows with equal lengths
    @x_col = 0     # column holding X points
    @y_col = 1     # column holding Y points
    self.load data_file
  end
  
  def length
    @data.length
  end
  
  def x
    @data.map {|e| e[@x_col]}
  end
  
  def y
    @data.map {|e| e[@y_col]}
  end
  
  def load(data_file)
    @data_file = data_file
    File.foreach(@data_file) do |line|
      next if line[0] == "#"
      @data << line.split.map {|e| e.to_f}
    end
  end
  
  def integrate(limits=[-Float::INFINITY, Float::INFINITY])
    sum = 0
    1.upto(@data.length-1) {|i|
      p0 = @data[i-1]
      p1 = @data[i]
      next if p1[@x_col] <= limits[0]
      sum += (p1[@y_col] + p0[@y_col]) * (p1[@x_col] - p0[@x_col]) / 2.0
      break if p1[@x_col] >= limits[1]
    }
    return sum
  end
  
  def inspect
    "File: #{@data_file}, lines: #{self.length} columns: #{@data[0].length} x: #{@x_col}, y: #{@y_col}"
  end
  
end


if $0 == __FILE__ then
  pw = DataFile.new(File.expand_path("../data.txt", File.dirname(__FILE__)))

  p pw.data
  p pw
  p pw.integrate
  p pw.integrate
end