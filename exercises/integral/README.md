# Excercise

Write a ruby class named `DataFile` that reads a file containing tabulated data as in the provided example file `data.txt`. The data in the provided file represents time (first column) and absorbed power (second and third column).

The `DataFile` class must have at least the following methods:

- `initialize` taking as argument the name of the file to be loaded
- `load`, with the same argument as `initialize`: it reads the data file into the `@data` variable, which must be an array of arrays (one array for each line in the original file)
- `integrate` taking as optional argument an array of two elements representing the extremes of integration. The integral shall be calculated by using the trapezoidal rule.

The `DataFile` class must be loaded and used by a separate script that reads the sample `data.txt` and writes another file with two columns: the same time and the incremental integral from `t=0` up to the current time.