#!/usr/bin/env ruby

require_relative "./lib/data_file.rb"

unless ARGV[0] then
  warn "Missing argument file"
  exit
end

file_name, file_ext = ARGV[0].split(".")

data = DataFile.new(ARGV[0])

p data
puts "Integral: #{data.integrate}"

xs = data.x
puts "Integrating from #{xs.min} to #{xs.max} in steps"
File.open("#{file_name}_out.#{file_ext}", "w") do |of|
  xs.each do |x|
    of.puts "#{x} #{data.integrate([xs.min, x])}"
  end
end

puts "Output written to #{file_name}_out.#{file_ext}"