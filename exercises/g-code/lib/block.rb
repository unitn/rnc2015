#!/usr/bin/env ruby

# 1. Block number (N)
# 2. Rapid and linear movements
# 3. X, Y, and Z coordinates
# 4. Tool number (T)
# 5. Feed rate (F)
# 6. Spindle rotational speed (S)
# 7. Machine commands (M)

class Block
  attr_accessor :n, :g, :x, :y, :z
  attr_accessor :tool, :feed_rate, :spindle_speed, :m
  
  def initialize(hsh = nil)
    raise ArgumentError, "Need a Hash" unless hsh.kind_of? Hash
    @n             = (hsh[:n] || 0)
    @g             = hsh[:g]
    @x             = hsh[:x]
    @y             = hsh[:y]
    @z             = hsh[:z]
    @tool          = hsh[:tool]
    @feed_rate     = hsh[:feed_rate]
    @spindle_speed = hsh[:spindle_speed]
    @m             = hsh[:m]
  end
  
  def modal(previous)
    raise ArgumentError, "Need a Hash" unless previous.kind_of? self.class
    @n             = (previous.n || 0) + 1
    @g             = (self.g             || previous.g            )
    @x             = (self.x             || previous.x            )
    @y             = (self.y             || previous.y            )
    @z             = (self.z             || previous.z            )
    @tool          = (self.tool          || previous.tool         )
    @feed_rate     = (self.feed_rate     || previous.feed_rate    )
    @spindle_speed = (self.spindle_speed || previous.spindle_speed)
    @m             = (self.m             || previous.m            )
    return self
  end
  
  def inspect
     "%4d:G%02d:M%02d (%9.3f,%9.3f,%9.3f) T%02d F%5d S%5d" % [
      @n             || 0,
      @g             || 0,
      @m             || 0,
      @x             || 0,
      @y             || 0,
      @z             || 0,
      @tool          || 0,
      @feed_rate     || 0,
      @spindle_speed || 0
    ]
  end
  
end


