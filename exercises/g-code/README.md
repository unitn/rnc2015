# Exercise

Write a ruby class that represents a block in a G-code program. the class must provide a container for all the data possibly conveyed by a symplified G-code that supports:

1. Block number (N)
2. Rapid and linear movements
3. X, Y, and Z coordinates
4. Tool number (T)
5. Feed rate (F)
6. Spindle rotational speed (S)
7. Machine commands (M)

The class must provide a functionality for *modal* combination of conseqent blocks: i.e. it must be able to inherit from the previous block the instructions that have not been explicitely defined for the current block. The modal behavior must support the functions 2--6 in the previous list, while the M instructions shall be collected into a common Array.

An example usage is as follows:

```ruby
blocks = []
blocks << Block.new({g:0, x:0, y:100, z:20, feed_rate:1500, spindle_speed:5000, tool:1})
blocks << Block.new({g:1, m:30, x:100}).modal(blocks[-1])
blocks << Block.new({y:0}).modal blocks[-1]
```

where the initialization has an optional second argument that represents a previous block to be inherited. This method shall in turn call an explicit method that implements the modal behavior. Finally, a proper `inspect` method shall be implemented, so that the call

```ruby
blocks.each {|b| p b}
```

prints out a complete descrition of the command sequence.

The class *is not required to provide the parsing mechanism*.
