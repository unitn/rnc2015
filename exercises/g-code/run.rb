#!/usr/bin/env ruby
require_relative "./lib/block.rb"

blocks = []
blocks << Block.new({g:0, x:0, y:100, z:20, feed_rate:1500, spindle_speed:5000, tool:1})
blocks << Block.new({g:1, x:100}).modal(blocks[-1])
blocks << Block.new({y:0}).modal(blocks[-1])

blocks.each {|b| p b}
