Introduction to bash shell
==========================

Linux and Unix operating systems have a powerful interface named *shell*. The
shell is typically executed within an application, called *terminal emulator*.
Different user interfaces (Gnome, KDE, etc) have different terminal emulators.
On Debian with Gnome interface the default terminal application is
unsurprisingly called `gnome-terminal` (or simply Terminal in the menu).

So, launch the Terminal application and follow the rest of this short guide.


Basics
------

When you first launch the Terminal, you get an empty window with a single line 
that says something similar to (it might be slightly different in your case):

    rnc@rnc15:~/rnc2015$ 
    
This is called the **prompt**. By default it has the following syntax: 
`[user name]@[machine name]:[path]$ `. The path component is important: it tells 
you *where are you*, i.e. what is your **current path**. 
  
To understand this point, we first have to define what the **path** is. All
the files on your disk are organized hierarchically in a tree-like structure.
Every node in this tree is a **foder** (or directory). Every file or directory
can be univocally identified with its path, or the sequence of nodes going from
the **root** of the tree to the file itself. On Unix/Linux systems, the root
element is represented by a single *forward* slash: `/`. A valid path starts
with the `/`, ends with the file name, and in between has the sequence of
folders separated by slashes. For example:

    /home/rnc/Desktop/test_file.txt

is the **full path** of the file `test_file.txt` that is into the `Desktop`
folder, in turn into `rnc`, in turn into `home`, which is an immediate child of
the root folder `/`.

Note that every user has a **home folder** with its own username within the
common folder `/home`. So in the previous example, `/home/rnc` is simply the
home folder of the user named `rnc`.

In order to limit damages, unexperienced users shall only change files under
their home directory. Given the importance of the home directory, there is a
shortcut for it: the tilda character `~` (on italian keyboards you get it by
typing Alt Gr+ì). So the shorter version for the previous example is:

    ~/Desktop/test_file.txt
    
Getting back to our terminal window, the prompt line is *prompting* for an
instruction, or a **command**, and a command is typically the **path** of an
executable file, followed by a **space-separated** list of options. 

The string made by the command plus the list of options is named 
**command line**.

The command must be one of the following cases:

  1. a full path of an executable file
  2. a *relative* path of an executable file
  3. the name of an executable file that is into a special system folder

Case 1. is easy: if you have an executable file named `myProgram` on your own
Desktop, the command line can be:

    rnc@rnc15:~/rnc2015$ ~/Desktop/myProgam
    
where the shortcut for the home folder has been used.

Case 2. introduces the concept of **relative path**: it tells the position of a
file *relative* to the current path (the one in the prompt), using two special
shortcuts: `./` (the current directory), and `../` (the parent of the current
directory). So the example would be:

    rnc@rnc15:~/rnc2015$ ../Desktop/myProgam
    
which means: move one folder up to the current directory (`~/rnc2015`), then get
`Desktop/myProgam`.

So a valid path starts either with a `/` (and it is absolute) or with a `.` (and
it is relative).

Case 3. introduces the concept of **system path**: if the command name is not a
path (absolute or relative), the shell searches within a list of pre-defined
system directories for an executable file with the same name. This list of
system directories can be obtained by typing the following command line after
the prompt and typing return (for brevity, the prompt before `$` is omitted):

    $ echo $PATH
    
where `echo` is a command, and `$PATH` is an argument. Together they make up a
command line.

--------------------------------------------------------------------------------

File management
---------------

### Name of current directory
Let's start using the terminal window for basic file management. The first
command we use shows the **name of the current directory** (`pwd` stands for
*print working directory*):

    $ pwd
    /home/rnc
 
The system is responding giving the full path of the current directory (in this
case the home of the user `rnc`).

### Content of a directory
Now we want to look at **the content of this directory**, and we use the command
`ls` (short for *list*):

    $ ls
    Desktop    Downloads  Pictures  Videos
    Documents  Music      Public    Templates

We can have more details about these objects by calling again the `ls` command
with an option:

    $ ls -l
    total 36
    drwxr-xr-x 2 rnc rnc 4096 Oct  2 14:54 Desktop
    drwxr-xr-x 2 rnc rnc 4096 Oct  2 14:54 Documents
    drwxr-xr-x 2 rnc rnc 4096 Oct  2 16:13 Downloads
    drwxr-xr-x 2 rnc rnc 4096 Oct  2 14:54 Music
    drwxr-xr-x 2 rnc rnc 4096 Oct  2 14:54 Pictures
    drwxr-xr-x 2 rnc rnc 4096 Oct  2 14:54 Public
    drwxr-xr-x 5 rnc rnc 4096 Oct 26 13:44 rnc2015
    drwxr-xr-x 2 rnc rnc 4096 Oct  2 14:54 Templates
    drwxr-xr-x 2 rnc rnc 4096 Oct  2 14:54 Videos

From right to left, each element is listed with its name, date of last
modification, group name and user name (both `rnc` here), and *access rights
scheme*. Explanation of the latter is here omitted for brevity, but we should
note that:

  - the first letter is `d` for directories, `-` for simple files;
  - the following letters represents your rights for *Reading*, *Writing*, and
    *eXecuting* the given file. If one of these letters is missing (replaced
    with a `-`), you are not allowed to perform that operation.
    
It should be noted that directories are always executables (an `x` at the 4th
character), as well as programs and executable files.

**NOTE**: if you type the up arrow cursor on your keyboard, the current command
line is replaced with the last executed command. If you type it repeatedly, you
can browse (up and down) the whole history of commands. Once you have selected a
command line, you can move the cursor back and forth and edit or correct its
options.


### Create a new directory
To **create a new directory**, we use the command `mkdir` (short for *make
directory*), which wants one argument representing the name of the directory
that we want to create:

    $ mkdir rnc2015
    $ ls
    Desktop    Downloads  Pictures  rnc2015    Videos
    Documents  Music      Public    Templates

### Directory navigation
To **move into the directory** that we just created, we use the `cd` command
(short for *change directory*). This command accepts the following arguments:

  - nothing: it moves to the hser home directory `~`
  - an absolute path
  - a relative path
  - a *naked* path, i.e. a path without any leading relative pointer (`./` or
    `../`): this is assumed to be a child of the current folder

As an example of the last case, we can move into the newly created directory
like this:

    $ cd rnc2015
    

### Create an empty file
We can **create a new, empty file** with the command `touch`:

    $ touch test
    $ mkdir MySubDir
    $ ls -l
    total 4
    drwxr-xr-x 2 rnc rnc 4096 Oct 26 14:20 MySubDir
    -rw-r--r-- 1 rnc rnc    0 Oct 26 14:20 test
 
Note that the size of `test` is 0 bytes.


### Move an item into a different position
Now we want to **move a file into a different position**, using the command
`mv`:

    $ mv test MySubDir
    $ ls MySubDir
    test
    $ ls -l
    total 4
    drwxr-xr-x 2 rnc rnc 4096 Oct 26 14:23 MySubDir
    

### Copy an item into a different position
A file (or a directory) can be **copied** by using the `cp` command, which wants
two arguments as the `mv` command does: the first argument is the source, the
second is the destination.

    $ cp MySubDir/test ./
    $ ls
    MySubDir  test
    
You can also use `cp` for duplicating a file:

    $ cp test anotherTest
    $ ls
    anotherTest  MySubDir  test
    
Finally, if you want to **delete a file**, you use the `rm` command (short for
*remove*):

    $ rm anotherTest


### Delete or remove a file (or a directory)
In order to **delete a directory**, you have to also specify two *command line
options* to the `rm` command:
    
    $ rm -rf MySubDir
    
Command line options are modifiers that change the way a command operate, and
are typically a list of letters (the order does not matter), with a leading `-`.
In this case the two letters are `f` for *force* (because deleting a directory
is a potentially dangerous operation) and `r` for *recursive* (i.e. also remove
all the contents including subdirectories).
    
### Edit a (text) file
You can quickly edit a text file (or an empty one) by using the command `gedit`:

    $ gedit test &
    
This opens the Gnome-editor window and load the `test` file in it. Since in our
case it is empty, you will get an empty window. You can make the changes you
want and then close the editor window.

The trailing `&` is not mandatory, but if you omit it, the terminal will become
unresponsive until the editor window is closed. The `&` runs the previous
command **in background**, allowing you to continue operating on the terminal.


