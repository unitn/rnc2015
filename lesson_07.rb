#!/usr/bin/env ruby

# Ruby Proc objects


def test(n)
  my_fun = proc do |x|
    x * n
  end
  return my_fun
end
