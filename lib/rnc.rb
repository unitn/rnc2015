#!/usr/bin/env ruby

# Ruby Numerical Control Library

# Avoid name clashes by using MODULES
module RNC
  AXES = {:X => 0, :Y => 1, :Z => 2}
  COMMANDS = [:G00, :G01]
  
  class Point < Array
  
    def self.[](x=nil, y=nil, z=nil)
      point = RNC::Point.new
      point[:X] = x
      point[:Y] = y
      point[:Z] = z
      return point
    end

    def -(other)
      return Math::sqrt(
        (self[:X] - other[:X]) ** 2 +
        (self[:Y] - other[:Y]) ** 2 +
        (self[:Z] - other[:Z]) ** 2
      )
    end
    
    def delta(other)
      result = Point.new
      AXES.keys.each do |axis|
        result[axis] = self[axis] - other[axis]
      end
      return result
    end
    
    def modal!(other)
      AXES.keys.each do |axis|
        self[axis] = other[axis] unless self[axis]
      end
    end
    
    def [](idx)
      super(remap_index(idx))
    end
    
    def []=(idx, val)
      super(remap_index(idx), val)
    end
    
    
    def inspect
      return "[#{self[:X].round(3) or '*'}, #{self[:Y].round(3) or  '*'}, #{self[:Z].round(3) or '*'}]"
    end
    
    private
    def remap_index(idx)
      case idx
      when Numeric
        return idx.to_i
      when Symbol
        return AXES[idx]
      when String
        return AXES[idx.upcase.to_sym]
      else
        raise "Point index must be a number or a string or a symbol"
      end
    end
    
  end # class Point
  
  
  class Block
    attr_reader :line
    attr_reader :start, :target, :feed_rate
    attr_reader :spindle_rate, :length, :type, :delta
    attr_accessor :profile, :dt
    
    def initialize(l="G00 X0 Y0 Z0 F1000")
      @start = Point[]
      @target = Point[]
      @feed_rate = nil
      @delta = nil
      @spindle_rate = nil
      @type = nil
      @length = nil
      @profile = nil
      @dt = nil
      self.line = l
    end
    
    def line=(l)
      raise ArgumentError, "Need a String!" unless l.kind_of? String
      @line = l.upcase # "g00".upcase becomes "G00"
      self.parse
    end
    
    def parse
      tokens = @line.split # tokens is an Array of substrings
      @type = tokens.shift.to_sym # either :G01 or :G00
      unless COMMANDS.include? @type then
        raise RuntimeError, "Unsupported command #{@type}"
      end
      tokens.each do |t|
        # t is a string representing each command
        # t[0] is the command type (X, Y, Z, F, S...)
        # t[1..-1] is the command argument (a number)
        cmd, arg = t[0], t[1..-1].to_f
        case cmd
        when "F"
          @feed_rate = arg
        when "S"
          @spindle_rate = arg
        when "X", "Y", "Z"
          @target[cmd] = arg
        else
          raise RuntimeError, "Unsupported G-Code command #{cmd}"
        end
      end
    end
    
    
    def modal!(prev_block)
      raise ArgumentError, "Need a Block!" unless prev_block.kind_of? Block
      @start = prev_block.target
      @target.modal! @start
      @feed_rate ||= prev_block.feed_rate
      @spindle_rate ||= prev_block.spindle_rate
      @length = @target - @start
      @delta  = @target.delta(@start)
      return self
    end
    
    
    def inspect
      return "[#{@type} #{@target} L#{@length or '*'} F#{@feed_rate} S#{@spindle_rate}]"
    end
  
  end # class Block
  
  
  class Parser
    attr_reader :blocks
    
    def initialize(cfg)
      raise ArgumentError unless cfg.kind_of? Hash
      raise ArgumentError, "Missing file name" unless cfg[:file]
      @cfg      = cfg
      @blocks   = [Block.new()]
      @profiler = Profiler.new(cfg)
      @interp   = Interpolator.new(@cfg)
    end
    
    def file_name; return @cfg[:file]; end
    
    def parse_file
      File.foreach(@cfg[:file]) do |line|
        next if line.length <= 1
        next if line[0] == "#"
        b = Block.new(line)
        b.modal!(@blocks.last)
        b.profile = @profiler.velocity_profile(b.feed_rate, b.length)
        # later on, we can call b.profile.call(t)
        b.dt      = @profiler.dt
        @blocks << b
      end
    end
    
    def each_block
      raise ArgumentError, "Need a block!" unless block_given?
      @blocks.each_with_index do |b, i|
        @interp.block = b
        yield @interp, i
      end
    end
    
    def inspect
      result = ""
      @blocks.each_with_index do |b, i|
        result << "#{i}: #{b.inspect}\n"
      end
      return result
    end
    
  end # class Parser
  
  
  class Profiler
    attr_reader :dt, :accel, :feed_rate, :times
    def initialize(cfg) # cfg must be a has with keys :A, :D
      raise ArgumentError unless cfg.kind_of? Hash
      raise ArgumentError, "Missing acceleration" unless cfg[:A]
      raise ArgumentError, "Missing deceleration" unless cfg[:D]
      raise ArgumentError, "Missing sampling time" unless cfg[:tq]
      @cfg = cfg
      @dt = 0.0
      @accel = []
      @feed_rate = 0.0
      @times = []
    end
    
    def velocity_profile(f_m, l)
      f_m /= 60.0
      l = l.to_f
      # Nominal time intervals (before time quantization)
      dt_1 = f_m / @cfg[:A] # @cfg[:A] > 0
      dt_2 = f_m / @cfg[:D] # @cfg[:D] > 0
      dt_m = l / f_m - (dt_1 + dt_2) / 2.0
      
      if dt_m > 0 then # Trapezoidal velocity profile
        q = quantize(dt_1 + dt_m + dt_2, @cfg[:tq])
        dt_m += q[1]
        f_m = (2 * l) / (dt_1 + dt_2 + 2 * dt_m)
      else # Triangular velocity profile
        dt_1 = Math::sqrt(2 * l / (@cfg[:A] + @cfg[:A] ** 2 / @cfg[:D]))
        dt_2 = dt_1 * @cfg[:A] / @cfg[:D]
        q = quantize(dt_1 + dt_2, @cfg[:tq])
        dt_m = 0.0
        dt_2 += q[1]
        f_m = 2 * l / (dt_1 + dt_2)
      end
      a = f_m / dt_1
      d = -(f_m / dt_2)
      
      @times = [dt_1, dt_m, dt_2]
      @accel = [a, d]
      @feed_rate = f_m
      @dt = q[0]
      
      return proc do |t|
        r = 0.0
        if t < dt_1 then # acceleration
          type = :A
          r = a * t ** 2 / 2.0
        elsif t < (dt_1 + dt_m) then # cuise
          type = :M
          r = f_m * (dt_1 / 2.0 + (t - dt_1))
        else # deceleration
          type = :D
          t_2 = dt_1 + dt_m
          r = f_m * dt_1 / 2.0 + f_m * (dt_m + t - t_2) + d / 2.0 * (t ** 2 + t_2 ** 2) - d * t * t_2
        end
        {:lambda => r / l, :type => type}
      end
    end
    
    private
    def quantize(t, dt)
      # quantize(1.0, 0.3) => [1.2, 0.2]
      result = []
      if (t % dt) == 0 then
        result = [t, 0.0]
      else
        result[0] = ((t / dt).to_i + 1) * dt
        result[1] = result[0] - t
      end
      return result
    end
  
  end # class Profiler
  
  
  class Interpolator
    attr_accessor :block
    def initialize(cfg)
      @cfg = cfg
      @block = nil
    end
    
    def each_timestep
      raise ArgumentError, "This method needs a block" unless block_given?
      t = 0.0
      while (cmd = self.eval(t)) do 
        yield t, cmd
        t += @cfg[:tq]
      end
    end
    
    def eval(t)
      result = {}
      case @block.type
      when :G00
        result[:position] = @block.target
        result[:lambda]   = 0.0
        result[:type]     = :R
      when :G01
        if t > @block.dt or t < 0 then
          return nil
        end
        result = @block.profile.call(t)
        # now result is {:lambda => 0.1, :type => :A}
        result[:position] = Point[]
        # now result is {:lambda => 0.1, :type => :A, :position => Point[]}
        # X(t) = X(0) + lambda * Delta_X
        [:X, :Y, :Z].each do |axis|
          result[:position][axis] = @block.start[axis] + result[:lambda] * @block.delta[axis]
        end
      else
        raise  RuntimeError, "Unexpected/usupported command #{@block.type}"
      end
      
      return result
    end
  
  end # class Interpolator

end # module RNC



# check whether the current file name is equal to the executable name:
# __FILE__ is the current file name
# $0 is the name of the main executable script
# ALSO, test the library with shell command pry -r ./filename.rb
if $0 == __FILE__ then
  # testing code here
  
  p1 = RNC::Point[0, 10, 10]
  p2 = RNC::Point[nil, 0, 20]
  
  p p1 # expect [0, 10, 10]
  p p2 # expect [*, 0, 20]
  
  p2.modal! p1
  p p2 # expect [0, 0, 20]
  
  puts "p1 - p2 = #{(p1 - p2).round(3)}"
  puts "p1.delta(p2): #{p1.delta p2}"

end
