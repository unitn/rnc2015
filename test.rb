#!/usr/bin/env ruby
# The line above is called "SHEBANG"

# Printing messages:
print("Hello, World!\n")
print "no parentheses!\n"

# Print a message plus a newline
puts "No newline this time!"

# Numbers: integers
puts 2 + 2
puts 11 / 2

# Numbers: reals
puts 11 / 2.0

# Variables
a = 11 / 2.0
print "a = ", a, "\n"  # Complex syntax, never used
a = 0
puts "a = #{a}"        # Much more common syntax

# Empty variable (nil)
b = nil
puts "b = #{b}"

# Constants: anything beginning with a Capital Letter
CONST = "This is a constant string"

puts CONST

CONST = "Changing value"

puts CONST


