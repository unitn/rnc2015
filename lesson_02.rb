#!/usr/bin/env ruby

# CLASSES and OBJECTS
#####################

# how to define a function:

def my_function(x, y=2)
  result = x * y
  return result
end

def greet(name="guys")
  puts "Hello, #{name}!"
end

puts my_function(12, 15)
puts my_function(10)

greet "Paolo"
greet
puts

# Class
# RESPECT THE CORRECT INDENTATION!!!
class Person # camel-case!
  attr_accessor :age
  attr_reader :name
  # attr_writer for write-only attributes
  
  def initialize(name, age)
    @name = name
    @age = age
  end # initialize
  
  def greet
    message = "Hello, my name is #{@name}!"
    puts message
  end # greet
  
  def print_age
    puts "#{@name}'s age is #{@age}"
  end # print_age
  
  def birthday
    @age = @age + 1
    puts "Happy birthday, #{@name}!!!"
  end

end # Person

me = Person.new("Paolo", 15)
me.greet
me.print_age
you = Person.new("John", 138)
you.greet
you.print_age

you.birthday
you.print_age

you.age = 25
puts "new age is #{you.age}"







