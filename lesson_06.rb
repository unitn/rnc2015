#!/usr/bin/env ruby
require "./lib/rnc.rb"

# How to deal with Files

CFG = {
  :file => "gcode.g",
  :A    => 500,
  :D    => 500,
  :tq   => 0.01
}

parser = RNC::Parser.new(CFG)
parser.parse_file


File.open("interp_test.txt", "w") do |f|
  f.puts "# idx time x y z lambda"
  parser.each_block do |interp, i|
    if interp.block.type == :G00 then
      puts "#{i} -: G00 #{interp.block.target}"
      f.puts "#{i} - #{interp.block.target[:X]} #{interp.block.target[:Y]} #{interp.block.target[:Z]} 0"
    elsif interp.block.type == :G01 then
      interp.each_timestep do |t, cmd|
        puts "#{i} #{t.round(3)}: #{cmd[:type]} #{cmd[:position]}"
        f.puts "#{i} #{t} #{cmd[:position][:X]} #{cmd[:position][:Y]} #{cmd[:position][:Z]} #{cmd[:lambda]}"
      end
    end
  end
end


